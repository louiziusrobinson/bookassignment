var express = require('express');
var router = express.Router();
const mongodb = require('mongodb');
const mongodbClient = require('mongodb').MongoClient;

const url = 'mongodb+srv://benben1209:hi@cluster0.p5s3l.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';

var options = {
  replSet: {
      sslValidate: false
  }
}

router.get('/', function(req, res, next) {
  let sqlCmd = ['SELECT rented.idrented, rented.renteddate, rented.rentedreturn, customers.customerfname, customers.customerlname, books.booktitle'
                +' From rented, books, customers WHERE rented.idbook = books.idbook AND rented.idcustomer = customers.idcustomer',
                'SELECT * FROM books',
                'SELECT * FROM customers'
  ]
   conn.query(sqlCmd.join(';'), (err, result) => {
      if (err) throw err;
    res.render('listRented', {
      data: JSON.stringify(result[0]),
      books: JSON.stringify(result[1]),
      customers: JSON.stringify(result[2])
    });
  });
});

router.delete('/delete/:idrented', (req, res) => {
  let idrented = req.params.idrented;
  console.log(idrented);
  let sqlCmd = 'DELETE FROM rented WHERE idrented = ?';
  conn.query(sqlCmd, idrented, (err,result) => {
    if (err) throw err;
    res.end();
  })
})

router.put('/update/:idrented', (req, res) => {
  let idrented = req.params.idrented;
  let form = req.body
  let sqlCmd = 'UPDATE rented SET ? WHERE idrented = ?';
  conn.query(sqlCmd, [form, idrented], (err, result) => {
    if(err) throw err;
  } )
  res.end();
})

module.exports = router;