var express = require('express');
var router = express.Router();
var formidable = require('formidable');
var multiparty = require('multiparty');

router.get('/', function(req, res, next) {
  res.render('addBooks');
});

router.post('/save', (req, res) => {
  const input = new formidable.IncomingForm();
  input.uploadDir = 'images';
  input.parse(req);
  input.on('fileBegin', (field, file) => {
    file.path = "./public/images/bookCover/" + file.name
  })
  var formData = new multiparty.Form();
  formData.parse(req, (error, fields, files) =>{
      var rowData =[
        fields.booktitle,
        fields.bookauthor,
        fields.bookGenre,
        files.bookimg[0].originalFilename
      ]
    
    let sqlCmd = 'INSERT INTO books(booktitle, bookauthor, bookGenre, bookimg) VALUES (?)'
    conn.query(sqlCmd, [rowData], (err, result) => {
      if (err) throw err;
      res.end();
      })
  });
})
module.exports = router;
