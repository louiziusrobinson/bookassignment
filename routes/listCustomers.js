var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  let sqlCmd = 'SELECT * FROM customers';
  conn.query(sqlCmd, (err, result) => {
    if (err) throw err;
    res.render('listCustomers', {
      data: JSON.stringify(result)
    });
  });
});

router.delete('/delete/:idcustomer', (req, res) => {
  let idcustomer = req.params.idcustomer;
  console.log(idcustomer);
  let sqlCmd = 'DELETE FROM customers WHERE idcustomer = ?';
  conn.query(sqlCmd, idcustomer, (err,result) => {
    if (err) throw err;
    res.end();
  })
})

router.put('/update/:idcustomer', (req, res) => {
  let idcustomer = req.params.idcustomer;
  let form = req.body
  let sqlCmd = 'UPDATE customers SET ? WHERE idcustomer = ?';
  conn.query(sqlCmd, [form, idcustomer], (err, result) => {
    if(err) throw err;
  } )
  res.end();
})

module.exports = router;