var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  let sqlCmd = ['SELECT * FROM books',
                'SELECT * FROM customers',
                'SELECT * FROM rented'
  
];
  conn.query(sqlCmd.join(';'), (err, result) => {
    if (err) throw err;
    res.render('addRented', {
      book: JSON.stringify(result[0]),
      customer: JSON.stringify(result[1]),
      rented: JSON.stringify(result[2])
    });
  });
});

router.post('/save', (req, res) => {
  let form = req.body;
  let sqlCmd = 'INSERT INTO rented SET ?'
  conn.query(sqlCmd, form, (err, result) => {
    if (err) throw err;
    res.end();
    })
});

module.exports = router;