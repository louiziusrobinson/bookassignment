var express = require('express');
var router = express.Router();
var formidable = require('formidable');
var multiparty = require('multiparty');

router.get('/', function(req, res, next) {
  let sqlCmd = ['SELECT * FROM books',
              'SELECT DISTINCT bookGenre FROM books',
              'SELECT DISTINCT bookauthor FROM books'];
  conn.query(sqlCmd.join(';'), (err, result) => {
    if (err) throw err;
    res.render('listBooks', {
      book: JSON.stringify(result[0]),
      genre: JSON.stringify(result[1]),
      author: JSON.stringify(result[2])
    });
  });
});

router.delete('/delete/:idbook', (req, res) => {
  let idbook = req.params.idbook;
  console.log(idbook);
  let sqlCmd = 'DELETE FROM books WHERE idbook = ?';
  conn.query(sqlCmd, idbook, (err,result) => {
    if (err) throw err;
    res.end();
  })
})

router.put('/update/:idbook', (req, res) => {
  let idbook = req.params.idbook;
  const input = new formidable.IncomingForm();
  input.uploadDir = 'images';
  input.parse(req);
  input.on('fileBegin', (field, file) => {
    file.path = "./public/images/bookCover/" + file.name
  })
  var formData = new multiparty.Form();
  formData.parse(req, (error, fields, files) =>{
      var rowData =[
        fields.booktitle,
        fields.bookauthor,
        fields.bookGenre,
        files.bookimg[0].originalFilename
      ]
    
    let sqlCmd = `UPDATE books SET booktitle = '${fields.booktitle}', bookauthor = '${fields.bookauthor}' , bookGenre = '${fields.bookGenre}', bookimg = '${files.bookimg[0].originalFilename}' WHERE idbook = ?`;
  conn.query(sqlCmd,[idbook], (err, result) => {
    if(err) throw err;
    res.end();
  })
  });
})
 
module.exports = router;