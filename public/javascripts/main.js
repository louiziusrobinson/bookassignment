let bookCover = ' ';
let ebookCover = ' ';

$('#btn-submit-addBooks').on('click', function (e){
    var letterNumber = /^[0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+$/;
    if (bookCover == ''|| $('#bookTitle').val() == '' || $('#authorName').val() == '' ||$('#genre').val() == '' ) {
        $('#bookTitle').val('');
        $('#authorName').val('');
        $('#genre').val('');
        $('#fileImage').val('');
        var image = document.getElementById('imageBookCover');
        image.src = '';

        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>All Information Need To Be Fill</h4>
                    </div>`
        $('#alerts').html(alert);
       
        
    }
    else if ($('#authorName').val().trim().match(letterNumber) || $('#authorName').val().trim().indexOf(' ') < 0) {
        $('#authorName').val('');
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>Please Put The Author Name</h4>
                    </div>`
        $('#alerts').html(alert);

    }
    else if ($('#genre').val().trim().match(letterNumber)) {
        $('#genre').val('');
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>Please Put the Genre Of the Book</h4>
                    </div>`
        $('#alerts').html(alert);
    }
    else{
        
        let books = new FormData();
        books.append('booktitle', $('#bookTitle').val().trim());
        books.append('bookauthor', $('#authorName').val().trim());
        books.append('bookGenre', $('#genre').val().trim());
        books.append('bookimg', $('input[type=file]')[0].files[0]);
        
        console.log(books)
        $.ajax({
            type: 'POST',
            url: '/addBooks/save',
            data: books,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: (data, status) => {
                if(status === 'success') {
                    $('#bookTitle').val('');
                    $('#authorName').val('');
                    $('#genre').val('');
                    $('#fileImage').val('');
                    var image = document.getElementById('imageBookCover');
                    image.src = '';
                    let alert = `<div class="alert alert-dismissible alert-success">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Save!</h1>
                        <h4>The Book Is In The System</h4>
                    </div>`
                    $('#alerts').html(alert);

                }
            }
        });
    }
     
});

let deleteBook = (idbook) => {
    $.ajax({
        type: "DELETE",
        url: `/listBooks/delete/${idbook}`,
        success: function (data, status) {
            if(status === 'success') {
                console.log('Delete!');
                location.reload();
            }
        }
    });
}

let editBook = (bookData) => {
    
    $('#ebookTitle').val(bookData.booktitle)
    $('#eauthorName').val(bookData.bookauthor)
    $('#egenre').val(bookData.bookGenre)
    document.getElementById('eimageBookCover').src = '/images/bookCover/' + bookData.bookimg
    $('#eidbook').val(bookData.idbook)
    $('#BookEdit').modal('show')

}

$('#btn-submit-EditBook').on('click', function (e){
    if ($('#ebookTitle').val() == '' || $('#eauthorName').val() == '' ||$('#egenre').val() == '' ) {
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>All Information Need To Be Fill</h4>
                    </div>`
        $('#ealerts').html(alert);
    }
    else{
        let books = new FormData();
        books.append('booktitle', $('#ebookTitle').val().trim());
        books.append('bookauthor', $('#eauthorName').val().trim());
        books.append('bookGenre', $('#egenre').val().trim());
        books.append('bookimg', $('input[type=file]')[0].files[0]);
        
        let idbook = $('#eidbook').val()
        $.ajax({
            type: "PUT",
            url: `/listBooks/update/${idbook}`,
            data: books,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function (data, status) {
                if(status === 'success') {
                    location.reload();

                }
            }
        });
    $('#BookEdit').modal('hide')
    }
});

$('#btn-submit-addCustomers').on('click', function (e){
    var letterNumber = /^[0-9!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+$/;
    if ( $('#phone').val() == ''|| $('#fname').val() == '' || $('#lname').val() == '' ||$('#email').val() == '' ) {
        $('#phone').val('');
        $('#fname').val('');
        $('#lname').val('');
        $('#email').val('');

        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>All Information Need To Be Fill</h4>
                    </div>`
        $('#alerts').html(alert);
       
        
    }
    else if ($('#fname').val().trim().match(letterNumber) || $('#fname').val().trim().indexOf(' ') > 0) {
        $('#fname').val('');
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>Please Put Your First Name</h4>
                    </div>`
        $('#alerts').html(alert);

    }
    else if ($('#lname').val().trim().match(letterNumber) || $('#lname').val().trim().indexOf(' ') > 0) {
        $('#lname').val('');
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>Please Put Your Last Name</h4>
                    </div>`
        $('#alerts').html(alert);

    }
    else if ($('#email').val().trim().indexOf(' ') > 0 || $('#email').val().trim().includes('@') != 1 ||$('#email').val().trim().includes('.com') != 1) {
        $('#email').val('');
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>Please Put Your email</h4>
                    </div>`
        $('#alerts').html(alert);
    }
    else if ($('#phone').val().trim().length != 10 || isNaN($('#phone').val().trim())) {
        $('#phone').val('');
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>Please Phone Number</h4>
                    </div>`
        $('#alerts').html(alert);
    }
    else{

        let customerForm = {
            customerfname: $('#fname').val().trim(),
            customerlname: $('#lname').val().trim(),
            customeremail: $('#email').val().trim(),
            customerphone: $('#phone').val().trim(),
        }
        $.ajax({
            type: "POST",
            url: "/addCustomers/save",
            data: customerForm,
            success: function (data, status) {
                if(status === 'success') {
                    $('#fname').val('');
                    $('#lname').val('');
                    $('#email').val('');
                    $('#phone').val('');
                    let alert = `<div class="alert alert-dismissible alert-success">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Save!</h1>
                        <h4>Customer Is In The System</h4>
                    </div>`
                    $('#alerts').html(alert);

                }
            }
        });
    }
     
});

let deleteCustomer = (idcustomer) => {
    $.ajax({
        type: "DELETE",
        url: `/listCustomers/delete/${idcustomer}`,
        success: function (data, status) {
            if(status === 'success') {
                location.reload();
            }
        }
    });
}

let editCustomer = (customerData) => {
    
    $('#efname').val(customerData.customerfname)
    $('#elname').val(customerData.customerlname)
    $('#eemail').val(customerData.customeremail)
    $('#ephone').val(customerData.customerphone)
    $('#eidCustomer').val(customerData.idcustomer)
    $('#CustomerEdit').modal('show')

}

$('#btn-submit-EditCustomer').on('click', function (e){
    if ($('#efname').val() == '' || $('#elname').val() == '' ||$('#eemail').val() == '' ||$('#ephone').val() == '' ) {
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>All Information Need To Be Fill</h4>
                    </div>`
        $('#ealerts').html(alert);
    }
    else{
        let customerForm = {
                customerfname: $('#efname').val().trim(),
                customerlname: $('#elname').val().trim(),
                customeremail: $('#eemail').val().trim(),
                customerphone: $('#ephone').val().trim(),
        }
        let idcustomer = $('#eidCustomer').val()
        $.ajax({
            type: "PUT",
            url: `/listCustomers/update/${idcustomer}`,
            data: customerForm,
            success: function (data, status) {
                if(status === 'success') {
                    location.reload();

                }
            }
        });
    $('#CustomerEdit').modal('hide')
    }
});



let rentedBook = (Data) => {

    $('#InfoRented').modal('show');
    $('#bookTitle').val(Data.booktitle);
    $('#idbookRented').val(Data.idbook);


}

$('#btn-save-rent').on('click', function (e){
    if ($('#rentDate').val() == '' ||$('#returnDate').val() == '' ||$('#readerName').val() == '') {
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>All Information Need To Be Fill</h4>
                    </div>`
        $('#alerts').html(alert);
        $('#rentDate').val('')
        $('#returnDate').val('') 
        $('#readerName').val('') 
    } else {
        let rentData = {
            renteddate: $('#rentDate').val(),
            rentedreturn: $('#rentDate').val(),
            idcustomer: $('#readerName').val(),
            idbook: $('#idbookRented').val()
        }
        $.ajax({
            type: "POST",
            url: "/addRented/save",
            data: rentData,
            success: function (data, status) {
                if(status === 'success') {
                    $('#rentDate').val('')
                    $('#returnDate').val('') 
                    $('#readerName').val('') 
                    let alert = `<div class="alert alert-dismissible alert-success">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Save!</h1>
                        <h4>Rent Is In The System</h4>
                    </div>`
                    $('#alerts').html(alert);

                }
            }
        });
    }
})

let deleteRented = (idrented) => {
    $.ajax({
        type: "DELETE",
        url: `/listRented/delete/${idrented}`,
        success: function (data, status) {
            if(status === 'success') {
                location.reload();
            }
        }
    });
}

let editRented = (RentData) => {
    
    $('#erentDate').val(RentData.renteddate)
    $('#ereturnDate').val(RentData.rentedreturn)
    $('#ereaderName').val(RentData.idcustomer)
    $('#ebookTitle').val(RentData.idbook)
    $('#eidbookRented').val(RentData.idrented)
    $('#editRented').modal('show')

}

$('#btn-edit-rent').on('click', function (e){
    if ($('#erentDate').val() == '' || $('#ereturnDate').val() == '' ||$('#ereaderName').val() == '' ||$('#ebookTitle').val() == '' ) {
        let alert = `<div class="alert alert-dismissible alert-danger">
                        <button type="button" class="close" onclick="hideAlert()">&times;</button>
                        <h1 class="alert-heading">Error!</h1>
                        <h4>All Information Need To Be Fill</h4>
                    </div>`
        $('#ealerts').html(alert);
    }
    else{
        let rentform = {
                renteddate: $('#erentDate').val(),
                rentedreturn: $('#ereturnDate').val(),
                idcustomer: $('#ereaderName').val(),
                idbook: $('#ebookTitle').val(),
        }
        let idrented = $('#eidbookRented').val()
        console.log(rentform, idrented)
        $.ajax({
            type: "PUT",
            url: `/listRented/update/${idrented}`,
            data: rentform,
            success: function (data, status) {
                if(status === 'success') {
                    location.reload();

                }
            }
        });
    $('#CustomerEdit').modal('hide')
    }
});

var eloadFile = function(event) {    
    var eimage = document.getElementById('eimageBookCover');
    eimage.src = URL.createObjectURL(event.target.files[0]);
};

var loadFile = function(event) {
    var image = document.getElementById('imageBookCover');
    image.src = URL.createObjectURL(event.target.files[0]);
    };

function hideAlert(){
    $('#alerts').html('');
    $('#ealerts').html('');
}

function closeEditBook(){
    $('#BookEdit').modal('hide');
}

function closeEditCustomer(){
    $('#CustomerEdit').modal('hide');
}

$(document).ready(function () {
    $('#readerName').select2({
        placeholder: 'Choose...',
    })
    $('#ereaderName').select2({
        placeholder: 'Choose...',
    })
    $('#ebookTitle').select2({
        placeholder: 'Choose...',
    })
})

function closeInfo() {
    $('#InfoRented').modal('hide');
    $('#editRented').modal('hide');

}

