var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
require('dotenv').config();

var indexRouter = require('./routes/index');
var addBooksRouter = require('./routes/addBooks');
var addCustomersRouter = require('./routes/addCustomers');
var addRentedRouter = require('./routes/addRented');
var listCustomersRouter = require('./routes/listCustomers');
var listRentedRouter = require('./routes/listRented');
var listBooksRouter = require('./routes/listBooks');

global.conn = require('./dbconnection/dbconnect');
conn.connect((error) => {
  if(error) throw error;
  console.log('Connected to MySQL');
});

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile)

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/addBooks', addBooksRouter);
app.use('/addCustomers', addCustomersRouter);
app.use('/listCustomers', listCustomersRouter);
app.use('/listRented', listRentedRouter);
app.use('/listBooks', listBooksRouter);
app.use('/addRented', addRentedRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
